#include<graphics.h>
#include<conio.h>

//引用Windows Multimedia API
#pragma comment(lib,"Winmm.lib")

#define High 864               //游戏画面
#define Width  591

IMAGE img_bk;         //背景图片
int position_x, position_y;        //飞机位置
IMAGE img_planeNormall, img_planeNormal2;    //飞机图片

void startup()
{
	initgraph(Width, High);;
	loadimage(&img_bk, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\background.jpg");
	loadimage(&img_planeNormall, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeNormal_2.jpg");
	loadimage(&img_planeNormal2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeNormal_2.jpg");
	position_x = High * 0.7;
	position_y = Width * 0.5;
	BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);          //显示背景
	putimage(position_x - 50, position_y - 60, &img_planeNormall, NOTSRCERASE);      //显示飞机
	putimage(position_x - 50, position_y - 60, &img_planeNormal2,SRCINVERT);
	FlushBatchDraw();
	Sleep(2);
}

void updateWithoutInput()
{

}

void updateWithInput()
{
	MOUSEMSG m;              //定义鼠标信息
	while (MouseHit())                //这个函数用于检测当前是否有鼠标信息
	{
		m = GetMouseMsg();
		if (m.uMsg == WM_MOUSEMOVE)
		{
			position_x = m.x;
			position_y = m.y;
		}
	}
}

void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main()
{
	startup();
	while (1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover();
	return 0;
}