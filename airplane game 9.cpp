//利用数组enemy_x[EnemyNum],enemy_y[EnemyNum]存储多台敌机的位置，可以实现同时出现多台敌机的效果
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

#define High 15           //游戏画面尺寸
#define Width 25
#define EnemyNum 5       //敌机的个数

//全局变量
int position_x, position_y;  //飞机的位置
int enemy_x[EnemyNum], enemy_y[EnemyNum];  //敌机的位置
int canvas[High][Width] = { 0 };         //0为空格，1为飞机，2为子弹，3为敌机
int score;          //得分

void gotoxy(int x, int y)       //将光标移到到（x，y)位置
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle, pos);
}

void HideCursor() // 用于隐藏光标
{
	CONSOLE_CURSOR_INFO cursor_info = { 1, 0 };  // 第二个值为0表示隐藏光标
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}

void startup() //数据的初始化
{
	position_x = High - 1;
	position_y = Width / 2;
	canvas[position_x][position_y] = 1;
	int k;
	for (k = 0; k < EnemyNum; k++)
	{
		enemy_x[k] = rand() % 2;
		enemy_y[k] = rand() % Width;
		canvas[enemy_x[k]][enemy_y[k]] = 3;
	}
	score = 0;
	HideCursor();
}

void show()       //显示画面
{
	gotoxy(0, 0);           //光标移到原点位置，以下重画清屏
	int i, j;
	for (i = 0; i < High; i++)
	{
		for (j = 0; j < Width; j++)
		{
			if (canvas[i][j] == 0)
				printf(" ");  //输出空格
			else if (canvas[i][j] == 1)
				printf("*");       //输出飞机
			else if (canvas[i][j] == 2)
				printf("|");         //输出子弹
			else if (canvas[i][j]==3)
				printf("@");
		}
		printf("\n");
	}
	printf("得分：%3d\n", score);
	Sleep(20);
}

void updateWithoutInput()        //与用户输入无关的更新
{
	int i, j, k;
	for (i = 0; i < High; i++)
	{
		for (j = 0; j < Width; j++)
		{
			if (canvas[i][j] == 2)
			{
				for (k = 0; k < EnemyNum; k++)
				{
					if ((i == enemy_x[k]) && (j == enemy_y[k]))       //子弹击中敌机
					{
						score++;           //分数加一
						canvas[enemy_x[k]][enemy_y[k]] = 0;
						enemy_x[k] = rand() % 2;      //产生新的敌机
						enemy_y[k] = rand() % Width;
						canvas[enemy_x[k]][enemy_y[k]]=3;
						canvas[i][j] = 0;   //子弹消失
					}
				}
				canvas[i][j] = 0;     //子弹向上移动
				if (i > 0)
					canvas[i - 1][j] = 2;
			}
		}
	}
	static int speed = 0;
	if (speed < 20)
		speed++;

	for (k = 0; k < EnemyNum; k++)
	{
		if ((position_x == enemy_x[k]) && (position_y == enemy_y[k]))    //敌机撞击我机
		{
			printf("失败！\n");
			Sleep(3000);
			system("pause");
			exit(0);
		}
		if (enemy_x[k] > High)         //敌机跑出显示屏幕
		{
			canvas[enemy_x[k]][enemy_y[k]] = 0;
			enemy_x[k] = rand() % 2;
			enemy_y[k] = rand() % Width;
			canvas[enemy_x[k]][enemy_y[k]] = 3;
			score--;
		}
		if (speed == 20)
		{
			for (k = 0; k < EnemyNum; k++)
			{
				canvas[enemy_x[k]][enemy_y[k]] = 0;
				enemy_x[k]++;
				speed = 0;
				canvas[enemy_x[k]][enemy_y[k]] = 3;
			}
		}
	}
}

void updateWithInput()               //与用户输入有关的更新
{
	char input;            //判断是否有输入
	if (kbhit())
	{
		input = getch();
		if (input == 'a')
		{
			canvas[position_x][position_y] = 0;
			position_y--;            //位置左移
			canvas[position_x][position_y] = 1;
		}
		else if (input == 'd')
		{
			canvas[position_x][position_y] = 0;
			position_y++;            //位置右移
			canvas[position_x][position_y] = 1;
		}
		else if (input == 'w')
		{
			canvas[position_x][position_y] = 0;
			position_x--;            //位置上移
			canvas[position_x][position_y] = 1;
		}
		else if (input == 's')
		{
			canvas[position_x][position_y] = 0;
			position_x++;            //位置下移
			canvas[position_x][position_y] = 1;
		}
		else if (input == ' ')            //发射子弹
		{
			canvas[position_x - 1][position_y] = 2;             //发射子弹的初始位置在飞机的正上方
		}
	}
}

int main(void)
{
	startup();   //数据初始化
	while (1)
	{
		show();   //显示画面
		updateWithoutInput();                 //与用户输入无关的更新
		updateWithInput();                         //与用户输入有关的更新
	}
	return 0;
}
