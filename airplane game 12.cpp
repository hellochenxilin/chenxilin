//第二步按鼠标左键后飞机发射子弹
#include<graphics.h>
#include<conio.h>

//引用Windows Multimedia API
#pragma comment(lib,"Winmm.lib")

#define High 864           //定义画面尺寸
#define Width 591

IMAGE img_bk;               //背景图片
int position_x, position_y;        //飞机位置
int bullet_x, bullet_y;       //子弹位置
IMAGE img_planeNormal1, img_planeNormal2;        //飞机图片
IMAGE img_bullet1, img_bullet2;            //子弹图片

void startup()
{
	initgraph(Width, High);    
	loadimage(&img_bk, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\background.jpg");
	loadimage(&img_planeNormal1, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeNormal_1.jpg");
	loadimage(&img_planeNormal2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeNormal_2.jpg");
	loadimage(&img_bullet1, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\bullet1.jpg");
	loadimage(&img_bullet2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\bullet2.jpg");
	position_x = Width * 0.5;
	position_y = High * 0.7;
	bullet_x = position_x;
	bullet_y = -85;
	BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);                  //显示背景
	putimage(position_x - 50, position_y - 60, &img_planeNormal1, NOTSRCERASE);      //显示飞机
	putimage(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
	putimage(bullet_x - 7,bullet_y, &img_bullet1, NOTSRCERASE);              //显示子弹
	putimage(bullet_x - 7, bullet_y, &img_bullet2, SRCINVERT);
	FlushBatchDraw();
	Sleep(2);
}
void updateWithoutInput()
{
	if (bullet_y > -25)
		bullet_y = bullet_y - 3;
}

void updateWithInput()
{
	MOUSEMSG m;       //定义鼠标信息
	while (MouseHit())              //检测是否有鼠标的信息
	{
		m = GetMouseMsg();
		if (m.uMsg == WM_MOUSEMOVE)
		{
			position_x = m.x;      //飞机的位置等于鼠标所在的位置
			position_y = m.y;
		}
		else if (m.uMsg == WM_LBUTTONDOWN)
		{
			bullet_x = position_x;          //按下鼠标左键发射子弹
			bullet_y = position_y - 85;
		}
	}
}

void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main(void)
{
	startup();       
	while (1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover();
	return 0;
}