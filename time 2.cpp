//秒钟的转动
#include<graphics.h>
#include<conio.h>
#include<math.h>

#define High 480               //游戏画面尺寸
#define Width 640
#define PI 3.14159

int main(void)
{
	initgraph(Width, High);                   //初始化640*480的绘图窗口
	int center_x, center_y;              //中心点的坐标，也是钟表的中心
	center_x = Width / 2;
	center_y = High / 2;
	int secondLength;             //秒针的长度
	secondLength = Width / 5;

	int secondEnd_x, secondEnd_y;           //秒针的终点
	float secondAngle = 0;                 //秒针对应的角度

	while (1)
	{
		//由角度决定的秒针终点坐标
		secondEnd_x = center_x + secondLength * sin(secondAngle);
		secondEnd_y = center_y - secondLength * cos(secondAngle);

		setlinestyle(PS_SOLID, 2);          //画实线，宽度为2
		setcolor(WHITE);
		line(center_x, center_y, secondEnd_x, secondEnd_y);           //画秒针
		Sleep(1000);
		setcolor(BLACK);            //隐蔽前一帧的秒针
		line(center_x, center_y, secondEnd_x, secondEnd_y);  
		
		//秒针角度的变化
		secondAngle = secondAngle + 2 * PI / 60;      //一圈一共2*PI,一圈60秒，一秒钟秒针走过的角度为2*PI/60
	}
	getch();               //按任意键继续
	closegraph();
	return 0;
}