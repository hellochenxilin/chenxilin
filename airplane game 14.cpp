//判断胜负
#include<graphics.h>
#include<conio.h>
#include<math.h>

//引用Windows Multimedia API
#pragma comment (lib,"Winmm.lib")

#define High 864          //游戏画面尺寸
#define Width 591

IMAGE img_bk;              //背景图片
int position_x, position_y;     //飞机的位置
int bullet_x, bullet_y;       //子弹的位置
float enemy_x, enemy_y;           //敌机的位置
IMAGE img_planeNormal1, img_planeNormal2;              //飞机图片
IMAGE img_planeExplode1, img_planeExplode2;           //爆炸飞机图片
IMAGE   img_bullet1, img_bullet2;               //子弹图片
IMAGE img_enemyPlane1, img_enemyPlane2;           //敌机图片
int isExpolde = 0;               //飞机是否爆炸

void startup()
{
	initgraph(Width, High);
	loadimage(&img_bk, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\background.jpg");
	loadimage(&img_planeNormal1, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeNormal_1.jpg");
	loadimage(&img_planeNormal2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeNormal_2.jpg");
	loadimage(&img_bullet1, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\bullet1.jpg");
	loadimage(&img_bullet2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\bullet2.jpg");
	loadimage(&img_enemyPlane1, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\enemyPlane1.jpg");
	loadimage(&img_enemyPlane2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\enemyPlane2.jpg");
	loadimage(&img_planeExplode1, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeExplode_1.jpg");
	loadimage(&img_planeExplode2, "D:\\QQ文件\\随书资源\\第5章\\5.2 飞机大战\\飞机大战图片音乐素材\\planeExplode_2.jpg");
	position_x = Width * 0.5;
	position_y = High * 0.7;
	bullet_x = position_x;
	bullet_y = -85;
	enemy_x = Width * 0.5;
	enemy_y = 10;
	BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);           //显示背景
	if (isExpolde == 0)
	{
		putimage(position_x - 50, position_y - 60, &img_planeNormal1, NOTSRCERASE);     //显示飞机
		putimage(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
		putimage(bullet_x - 7, bullet_y, &img_bullet1, NOTSRCERASE);       //显示子弹
		putimage(bullet_x - 7, bullet_y, &img_bullet2, SRCINVERT);
		putimage(enemy_x, enemy_y, &img_enemyPlane1, NOTSRCERASE);     //显示敌机
		putimage(enemy_x, enemy_y, &img_enemyPlane2, SRCINVERT);
	}
	else
	{
		putimage(position_x - 50, position_y - 60, &img_planeExplode1, NOTSRCERASE);        //显示爆炸飞机
		putimage(position_x - 50, position_y - 60, &img_planeExplode2, SRCINVERT);
	}
	FlushBatchDraw();
	Sleep(2);
}

void updateWithoutInput()
{
	if (bullet_y > -25)
		bullet_y = bullet_y - 2;
	if (enemy_y < High - 25)
		enemy_y = enemy_y + 0.5;
	else
		enemy_y = 10;
	if (fabs(bullet_x - enemy_x) + fabs(bullet_y - enemy_y) < 50)    //子弹击中敌机
	{
		enemy_x = rand() % Width;
		enemy_y = -40;
		bullet_y = -85;
	}
	if (fabs(position_x - enemy_x) + fabs(position_y - enemy_y) < 150)          //敌机撞击我机
		isExpolde = 1;
}

void updateWithInput()
{
	MOUSEMSG m;       //定义鼠标消息
	while (MouseHit())
	{
		m = GetMouseMsg();
		if (m.uMsg == WM_MOUSEMOVE)
		{
			position_x = m.x;
			position_y = m.y;
		}
		else if (m.uMsg == WM_LBUTTONDOWN)
		{
			bullet_x = position_x;
			bullet_y = position_y - 85;
		}
	}
}

void gameover()
{
	EndBatchDraw();
	getch();
	closegraph();
}

int main(void)
{
	startup();
	while (1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover();
	return 0;
}