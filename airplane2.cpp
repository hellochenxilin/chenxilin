//静止的敌机
#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
//全局变量
int position_x, position_y;      //飞机位置
int bullet_x, bullet_y;            //子弹位置
int enemy_x, enemy_y;           //敌机位置
int high, width;              //游戏画面尺寸

void startup()           //数据的初始化
{
	high = 20;
	width = 30;
	position_x = high / 2;
	position_y = width / 2;
	bullet_y = -1;
	bullet_y = position_y;
	enemy_x = 0;
	enemy_y = position_y;
}

void show()           //显示画面
{
	system("cls");
	int i, j;
	for (i = 0; i < high; i++)
	{
		for (j = 0; j < width; j++)
		{
			if ((i == position_x) && (j == position_y))
				printf("*");         //输出飞机
			else if ((i == enemy_x) && (j == enemy_y))
				printf("@");            //输出敌机
			else if ((i == bullet_x) && (j == bullet_y))
				printf("|");                 //输出子弹
			else
				printf(" ");          //输出空格
		}
		printf("\n");
	}
}
void updateWithoutInput()         //与用户输入无关的更新
{
	if (bullet_x > -1)
		bullet_x--;
}

void updateWithInput()    //与用户输入有关的更新
{
	char  input;
	if (kbhit())
	{
		input = getch();
		if (input == 'a')                  //位置左移
			position_y--;               
		if (input == 'd')                      //位置右移
			position_y++;
		if (input == 'w')               //位置上移
			position_x--;
		if (input == 's')           //位置下移
			position_x++;
		if (input == ' ')                 
		{
			bullet_x = position_x - 1;          //发射子弹的初始位置在飞机的正上方
			bullet_y = position_y;
		}
	}
}
int main()
{
	startup();                //数据的初始化
	while (1)          //游戏循环执行
	{
		show();          //显示画面
		updateWithoutInput();   //与用户输入无关的更新
		updateWithInput();              //与用户输入有关的更新
	}
	return 0;
}