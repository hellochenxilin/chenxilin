//绘制静态秒针
#include<graphics.h>
#include<conio.h>
#include<math.h>

#define High 480             //游戏画面尺寸
#define Width 640

int main(void)
{
	initgraph(Width, High);         //初始化640*480的绘图窗口
	int center_x = Width / 2, center_y = High / 2;       //中心点的坐标，也是钟表的中心
	int seconLength = Width / 5;                      //秒针的长度
	int secondEnd_x = center_x + seconLength;            //秒针的终点
	int secondEnd_y = center_y;

	//画秒针
	setlinestyle(PS_SOLID, 2);         //画实线，宽度为2
	setcolor(WHITE);            //线条为白色
	line(center_x, center_y, secondEnd_x, secondEnd_y);
	getch();         //按任意键继续
	closegraph();
	return 0;
}