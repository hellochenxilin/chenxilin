//第三步定义系统变量（SYSTEMTIME ti),通过GetLocalTime(&ti)获取当前时间，秒针的角度由实际时间确定，即secondAngle=ti.wSencond*2*PI/60.
#include<graphics.h>
#include<conio.h>
#include<math.h>

#define High 480        //游戏画面尺寸
#define Width 640              
#define PI 3.14159

int main(void)
{
	initgraph(Width, High);                 //初始化640*480的绘图窗口
	int center_x, center_y;               //z中心点的坐标，也是钟表的中心
	center_x = Width / 2;
	center_y = High / 2;
	int secondLength;                       //秒针的长度
	secondLength = Width / 5;           
	int secondEnd_x, secondEnd_y;            //秒针的终点
	float secondAngle;            //秒针对应的角度
	SYSTEMTIME  ti;                   //定义变量保存当前的时间

	while (1)
	{
		GetLocalTime(&ti);              //获取当前的时间
		//秒针角度的变化
		secondAngle = ti.wSecond * 2 * PI / 60;                //一圈是2*PI，一圈60秒，一秒钟秒针走过的角度为2*PI/60
		//由角度决定的秒针端点坐标
		secondEnd_x = center_x + secondLength * sin(secondAngle);
		secondEnd_y = center_y - secondLength * cos(secondAngle);

		setlinestyle(PS_SOLID, 2);    //画实线，宽度为2
		setcolor(WHITE);
		line(center_x, center_y, secondEnd_x, secondEnd_y);           //画秒针

		Sleep(10);
		setcolor(BLACK);
		line(center_x, center_y, secondEnd_x, secondEnd_y);    //隐蔽前一帧的秒针
	}
	getch();
	closegraph();
	return 0;
}

